Human Decimal Formatter
------------------------

## About

A tiny module that provides a simple decimal field formatter
that displays decimal digits only if exist.

For example 3.00 will render as 3 (no digital digits) but 3.23 will render
as 3.23 etc.

Initially I found it helpful to propose this formatter on core but until this
happens it would be better to add this here as a separate module and get some
feedback from the community.

Scaffolding was made with [Drupal Console](https://drupalconsole.com).

## Usage

Just install and use the formatter for any decimal number field you want to.
There are no special settings for the formatter.
